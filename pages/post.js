import Layout from '../components/MasterLayout'
import fetch from 'isomorphic-unfetch'

const Post = (props) => (
    <Layout>
        <h1>{props.episode.name}</h1>
        <p>{props.episode.summary.replace(/<[/]?p>/g, '')}</p>
        <img src={props.episode.image.original} alt="" />
    </Layout>
)

Post.getInitialProps = async function (context) {
    const { id } = context.query
    const res = await fetch(`http://api.tvmaze.com/episodes/${id}`)
    const episode = await res.json()

    return {episode}
}

export default Post