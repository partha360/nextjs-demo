import Layout from '../components/MasterLayout'
import Link from 'next/link'
import fetch from 'isomorphic-unfetch'

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Card, { CardActions, CardContent, CardMedia } from 'material-ui/Card';
import Button from 'material-ui/Button';
import Grid from 'material-ui/Grid';
import Paper from 'material-ui/Paper';
import Dialog, {
    DialogTitle,
    DialogContent,
    DialogContentText,
    DialogActions,
} from 'material-ui/Dialog';
import Typography from 'material-ui/Typography';
import withStyles from 'material-ui/styles/withStyles';
import withRoot from '../components/withRoot';

const styles = theme => ({
    root: {
        textAlign: 'center',
        paddingTop: 5,
    },
    card: {
        maxWidth: 345,
    },
    media: {
        height: 200,
    },
    control: {
        padding: theme.spacing.unit * 2,
    },
});

class Index extends Component {
    state = {
        spacing: '16',
        open: false,
    };

    handleRequestClose = () => {
        this.setState({
            open: false,
        });
    };

    handleClick = () => {
        this.setState({
            open: true,
        });
    };

    render() {
        const { classes } = this.props;
        const { spacing } = this.state;

        return (
            <div className={classes.root}>
                <Layout>
                    <Typography type="display2" gutterBottom>
                        Game of Thrones Episodes
                    </Typography>
                    <Grid container className={classes.root}>
                        <Grid item xs={12}>
                            <Grid container className={classes.demo} justify="center"
                                spacing={Number(spacing)}>
                                {this.props.episodes.map((episode) => (
                                    <Grid key={this.props.value} item>
                                        <Card className={classes.card}>
                                            <CardMedia
                                                className={classes.media}
                                                image={episode.image.medium}
                                                title={episode.name}
                                            />
                                            <CardContent>
                                                <Typography type="headline" component="h2">
                                                    S{episode.season} Ep{episode.number}
                                                </Typography>
                                                <Typography type="headline" component="h3">
                                                    {episode.name}
                                                </Typography>
                                                <Typography component="p">
                                                    {episode.summary.replace(/<[/]?p>/g, '')}
                                                </Typography>
                                            </CardContent>
                                            <CardActions>
                                                <Button dense color="primary">
                                                    Share
                                            </Button>
                                                <Button dense color="primary">
                                                    Learn More
                                            </Button>
                                            </CardActions>
                                        </Card>
                                    </Grid>
                                ))}
                            </Grid>
                        </Grid>
                    </Grid>

                </Layout>
            </div>
        );
    }
}

Index.propTypes = {
    classes: PropTypes.object.isRequired,
};

Index.getInitialProps = async function () {
    const res = await fetch('http://api.tvmaze.com/singlesearch/shows?q=thrones&embed=episodes')
    const data = await res.json()
    const episodes = data._embedded.episodes
    return {
        episodes
    }
}

export default withRoot(withStyles(styles)(Index));