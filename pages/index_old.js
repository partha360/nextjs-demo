import Layout from '../components/MasterLayout'
import Link from 'next/link'
import fetch from 'isomorphic-unfetch'

const Index = (props) => (
    <Layout>
        <h1>Game of Thrones Episodes</h1>
        <ul>
            {props.episodes.map((episode) => (
                <li key={episode.id}>
                    <Link as={`/p/${episode.id}`} href={`/post?id=${episode.id}`}>
                        <a>
                            <img src={episode.image.medium} alt="" />
                            Season:{episode.season} Episode:{episode.number} {episode.name}
                        </a>
                    </Link>
                </li>
            ))}
        </ul>
        <style jsx>{`
            h1, a {
                font-family: "Calibri";
            }
            ul {
                padding: 0;
            }
            li {
                list-style: none;
                margin: 5px 0;
            }
            a {
                text-decoration: none;
                color: blue;
                font-size: 30px;
                font-style: bold;
            }
            a:hover {
                opacity: 0.6;
            }
        `}</style>
    </Layout>
)

Index.getInitialProps = async function () {
    const res = await fetch('http://api.tvmaze.com/singlesearch/shows?q=thrones&embed=episodes')
    const data = await res.json()
    const episodes = data._embedded.episodes

    return {
        episodes
    }
}

export default Index