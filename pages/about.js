import Layout from '../components/MasterLayout'

const content = (
    <Layout>
        <p> Hello from About page</p>
    </Layout>
)

export default () => (content)