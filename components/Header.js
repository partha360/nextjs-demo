/* eslint-disable flowtype/require-valid-file-annotation */
import Link from 'next/link'
import Router from 'next/router'

import React from 'react';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';
import Tabs, { Tab } from 'material-ui/Tabs';
import PhoneIcon from 'material-ui-icons/Phone';
import FavoriteIcon from 'material-ui-icons/Favorite';
import PersonPinIcon from 'material-ui-icons/PersonPin';
import withStyles from 'material-ui/styles/withStyles';
import withRoot from '../components/withRoot';

function TabContainer(props) {
    return <div style={{ padding: 20 }}>{props.children}</div>;
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
};

const styles = theme => ({
    root: {
        flexGrow: 1,
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        backgroundColor: theme.palette.background.paper,
    },
});

class Header extends React.Component {
    state = {
        value: 0,
    }

    handleChange = (event, value) => {
        this.setState({ value });
    }

    render() {
        const { classes } = this.props;
        const { value } = this.state;
        return (
            <div className={classes.root}>
                <Paper style={{ width: 500 }}>
                    <Tabs
                        value={value}
                        onChange={this.handleChange}
                        fullWidth
                        indicatorColor="accent"
                        textColor="accent"
                    >
                        <Tab icon={<PhoneIcon />} label="RECENTS">
                        </Tab>
                        <Tab icon={<FavoriteIcon />} label="FAVORITES">
                        </Tab>
                        <Tab icon={<PersonPinIcon />} label="NEARBY" />
                    </Tabs>
                </Paper>
                <Link href="/about">
                    <a>About</a>
                </Link>
                {value === 0 && <TabContainer>{'Item One'}</TabContainer>}
                {value === 1 && <TabContainer>{'Item Two'}</TabContainer>}
            </div>
        )
    }
}

Header.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withRoot(withStyles(styles)(Header));